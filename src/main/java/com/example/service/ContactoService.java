package com.example.service;

import java.util.List;

import com.example.bean.Comentario;

public interface ContactoService {
	
	List<Comentario> obtenerComentarios();
	
	void insertarComentario(Comentario comentario);

}
