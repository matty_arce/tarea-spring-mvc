package com.example.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.bean.Comentario;

@Service
public class ContactoServiceImpl  implements ContactoService{
	private static List<Comentario> comentarios;
	
	@Override
	public List<Comentario> obtenerComentarios() {
		if (comentarios == null)
			comentarios = new ArrayList<>();
		return comentarios;
	}

	@Override
	public void insertarComentario(Comentario comentario) {
		this.obtenerComentarios().add(comentario);
		
	}
	

}
