package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.service.ContactoService;

/*
 * TODO: Agregar anotaciones faltantes 
 */
public class ContactoController {
	
	
	/**
	 * Utilizar esta variable para obtener y almacenar datos
	 * los metodos a utilizar son
	 * obtenerComentarios e insertarComentario
	 */
	@Autowired
	private ContactoService contactoService;

	
	/*
	 * TODO: Agregar metodos para manejar peticiones. 
	 *  1. metodo para mostrar el index (vista inicial).
	 *  2. metodo para mostrar lista de comentarios registrados.
	 *  3. metodo para mostrar formulario de alta de comentarios. 
	 *  4. metodo para almacenar un comentario.  
	 * 
	 */
}
